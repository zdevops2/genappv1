## Setting up GenApp from Scratch on a zD&T as a Service System

* Request access to the zD&T as a Service Internal IBM Systems from __fultonm@ibm.com__
* You will get an IP address and initial userid/password for TSTRADM
* Edit `/etc/hosts` and create an entry to give your system a name, e.g.
```
9.20.209.42 fultonm.genapp.ibm.com
```

### Clone the provisioning tools and obtain the necessary software required for GenApp
 
#### On your Desktop System (e.g. Mac or Linux Desktop)
```
cd ~
mkdir -p GenApp/provisioning/tools
cd GenApp/provisioning/tools
```

* Go to: [Rocket Software](https://my.rocketsoftware.com/RocketCommunity/RCLogin) and download Rocket tools into the _tools_ directory. 
* These are the tools you should get (or newer versions)
```
 bash-4.3_b018.170518.tar.gz    git-2.14.4_b08.181016.tar.gz	 perl-5.24.0_b007.180202.tar.gz
 curl-7.52.1_b006.170926.tar.gz gzip-1.6-edc_b0005.160229.tar	 unzip-6.0_b0006.161123.tar.gz
```

* You can either use your own certificates, or you can use Mike Fulton's (_fultonm@ca.ibm.com_). See the links in the [readme](https://github.com/mikefultonbluemix/samples/blob/master/provisioning/README.md) if you want to use your own certificates.
```
cd ..
mkdir certificates
cd certificates
```

Clone the provisioning tools (part of samples - should probably be forked):
```
cd ..
git clone https://github.com/mikefultonbluemix/samples
cd samples/provisioning
```
(read _README.md_)
edit *setenv.sh*
* change _ROCKET_TOOLS_DIR_ to point to your local directory of rocket tools (e.g. _~/GenApp/provisioning/tools_)
* change _CERT_DIR_ to point to your local directory of z/OS Certificates (e.g. _~/GenApp/provisioning/certificates_)
* change _ZOS_HOST_ to the name of your zD&T z/OS system (e.g. _fultonm.genapp.ibm.com_)
* change _ZOS_GIT_USER_ to your git user name
* change _ZOS_GIT_EMAIL_ to your git email
* you can use the defaults for the other environment variables

#### Still on your Desktop System (e.g. Mac or Linux Desktop)
```cd ~/GenApp/provisioning/samples/provisioning```

```./bringupSystem.sh``` _(configure ssh, push tools to the system, set up git, git clone tools you need, build the tools, change default shell to be bash)_
 
_(You will be setting up your remote shell login. If you have done this in the past, it will prompt you if you want to overwrite your id_rsa - say 'n' (no). It will then prompt you twice for your password for TSTRADM on your z/OS system to configure ssh and then will be able to run unattended)_

```./configSMPE.sh``` _(set up SMP/E maintenance on your system)_

#### Set up your public key
You need to get a public key from your z/OS account and put it into your IBM GHE account so that you can git clone from GHE to z/OS

* Log on to your z/OS System, e.g. 
```
ssh tstradm@genapp.fultonm.ibm.com

ssh-keygen -t rsa -b 4096 -C "fultonm@ca.ibm.com"  _(specify passphrase)_
eval "$(ssh-agent -s)"
ssh-add  ~/.ssh/id_rsa _(specify passphrase)_
vi ~/.ssh/id_rsa.pub
_(copy contents into buffer)_
```

* Switch to your github settings on IBM GHE
* Go to [IBM github](https://github.ibm.com/settings/keys)
* create a _new ssh key_ and paste the contents in from your buffer
 
#### Clone GenApp 
* Switch back to z/OS
```
cd /zaas1
git clone git@github.ibm.com:IBMZSoftware/nazare-demo-genapp.git
git clone git@github.ibm.com:IBMZSoftware/nazare-demo-imsapp.git
git clone git@github.ibm.com:IBMZSoftware/nazare-demo-sysadmin.git
```
Note that as of this writing, you will need to checkout the ussbldpkg branch of sysadmin, so:
```
cd /zaas1/nazare-demo-imsapp
git fetch origin
git checkout -b ussbldpkg origin/ussbldpkg
```

* Set up the IBMUSER profile
  * log in as tstradm
  * ```cp .profile ~ibmuser``` (set up IBMUSER to have access to tools)
  * ```cp .devprofile ~ibmuser```
 
Now that IBMUSER is properly configured and we know the password, we can run the install script

#### Install GenApp
* ```cd /zaas1/nazare-demo-sysadmin/GenApp/build```
* ```./install.sh``` (install GenApp, compile the code, configure CICS, Db2, start Db2 and start CICS)
* If this is the first time you have installed genapp, the db2drop process will have a soft failure because there is nothing to drop
* Once install is complete, there will be instructions printed to the screen saying how to logon to CICS to run genapp trans
