LİSANS BİLGİLERİ

Aşağıda listelenen programlar, daha önce Müşteri ve IBM tarafından kabul edilen Program lisansı koşullarına ek olarak aşağıdaki Lisans Bilgilerinin kayıt ve koşulları kapsamında lisanslanır. Müşteri, Programa ilişkin yürürlükte olan lisans koşullarını daha önce kabul etmemişse, IBM Garanti Verilmeyen Programlar İçin Uluslararası Lisans Sözleşmesi (Z125-5589-05) geçerli olur.

Program Adı: IBM CICS TS GENAPP extension using Liberty Profile Version: 5 Release: 1
Program Numarası: SupportPac CB12

Sınırlı Kullanım Programı

Bu Program yalnızca aşağıda belirtilen Adı Belirlenmiş Program(lar)la veya bunların halefleriyle kullanılmak üzere sağlanır. Lisans Alan Tarafın, bu Programı diğer herhangi bir yazılımla bağlantılı olarak kullanması yasaktır.

Adı Belirlenmiş Program(lar):
IBM CICS Transaction Server for z/OS V5

Kaynak Bileşenler ve Örnek Malzemeler

Bu Program, kaynak kodu biçimindeki bazı bileşenleri ("Kaynak Bileşenler") ve Örnek Malzemeler olarak tanımlanan diğer malzemeleri içerebilir. Lisans Alan Taraf, Kaynak Bileşenleri ve Örnek Malzemeleri ancak Kaynak Bileşenleri ve Örnek Malzemeleri kullanımının bu Sözleşme kapsamındaki lisans hakları tarafından çizilen sınırlar içinde kalması ve ancak Kaynak Bileşenlerde ya da Örnek Malzemelerde içerilen hiçbir telif hakkı bilgisini veya bildirimini değiştirmemek veya silmemek koşuluyla dahili olarak kullanmak amacıyla kopyalayabilir ve kullanabilir. IBM, Kaynak Bileşenleri ve Örnek Malzemeleri, destek yükümlülüğü olmaksızın ve MÜLKİYETE, HAK İHLALİ YAPILMAYACAĞINA VEYA MÜDAHALE EDİLMEYECEĞİNE DAİR GARANTİLER İLE TİCARİLİĞE VE BELİRLİ BİR AMACA UYGUNLUĞA DAİR ZIMNİ GARANTİLER VE KOŞULLAR DA DAHİL OLMAK, ANCAK TÜMÜ BUNLARLA SINIRLI OLMAMAK ÜZERE, AÇIK VEYA ZIMNİ HİÇBİR GARANTİ VERMEKSİZİN "OLDUĞU GİBİ" ESASIYLA SAĞLAR.

L/N:  L-APIG-9RMFZQ
D/N:  L-APIG-9RMFZQ
P/N:  L-APIG-9RMFZQ 