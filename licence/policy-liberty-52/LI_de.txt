LIZENZINFORMATION

Für die Lizenzierung der nachstehend aufgelisteten Programme gelten zusätzlich zu den bereits zwischen dem Kunden und IBM vereinbarten Programmlizenzbedingungen die Bedingungen der folgenden Lizenzinformation. Falls der Kunde den für das Programm geltenden Lizenzbedingungen nicht bereits zugestimmt hat, kommt IBM Internationale Nutzungsbedingungen für Programme ohne Gewährleistung (Z125-5589-05) zur Anwendung.

Programmname: IBM CICS TS GENAPP extension using Liberty Profile Version: 5 Release: 1
Programmnummer: SupportPac CB12

Programm mit beschränktem Nutzungsrecht

Dieses Programm wird nur zur Nutzung mit den nachfolgend aufgeführten "genannten Programmen" oder ihren Nachfolgeprodukten bereitgestellt. Es ist dem Lizenznehmer nicht gestattet, dieses Programm in Verbindung mit anderer Software zu nutzen.

Genannte Programme:
IBM CICS Transaction Server for z/OS V5

Quellenkomponenten und Mustermaterialien

Das Programm kann einige Komponenten in Form von Quellcode ("Quellenkomponenten") und sonstige Materialien, die als Mustermaterialien gekennzeichnet sind, enthalten. Der Lizenznehmer darf die Quellenkomponenten und Mustermaterialien nur für den internen Gebrauch kopieren und ändern, sofern eine solche Verwendung im Rahmen der Lizenzrechte dieser Vereinbarung erfolgt und der Lizenznehmer die in den Quellenkomponenten oder Mustermaterialien enthaltenen Copyrightvermerke oder Hinweise weder ändert noch löscht. IBM stellt die Quellenkomponenten und Mustermaterialien ohne eine Verpflichtung zur Unterstützung, ohne Wartung (auf "as-is"-Basis) und ohne jegliche Gewährleistung (ausdrücklich oder stillschweigend) zur Verfügung, insbesondere ohne Gewährleistung für Rechtsmängel, für die Freiheit von Rechten Dritter, für das Recht auf Nichtbeeinträchtigung, für die Handelsüblichkeit und für die Verwendungsfähigkeit für einen bestimmten Zweck.

L/N:  L-APIG-9RMFZQ
D/N:  L-APIG-9RMFZQ
P/N:  L-APIG-9RMFZQ 