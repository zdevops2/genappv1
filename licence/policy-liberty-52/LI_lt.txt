LICENCIJOS INFORMACIJA

Toliau nurodytos Programos licencijuojamos pagal šias Licencijos informacijos sąlygas, kurios papildo Programos licencijos sąlygas, dėl kurių susitarė Klientas ir IBM. Jei Klientas anksčiau nėra sutikęs su Programai taikomomis licencijos sąlygomis, taikoma IBM Programų kurioms neteikiama garantija tarptautinė licencinė sutartis (Z125-5589-05).

Programos pavadinimas: IBM CICS TS GENAPP extension using Liberty Profile Version: 5 Release: 1
Programos numeris: SupportPac CB12

Ribotojo naudojimo Programa

Ši Programa pateikiama tik naudoti su toliau nurodyta (-omis) Programa (-omis) ar jų vėlesnėmis versijomis. Licenciatui draudžiama naudoti šią Programą kartu su bet kokia kita programine įranga.

Nurodyta (-os) Programa (-os):
IBM CICS Transaction Server for z/OS V5

Šaltinio komponentai ir pavyzdinė medžiaga

Į Programą gali būti įtraukti keli komponentai išeitinio kodo forma („Šaltinio komponentai“) ir kita medžiaga, apibrėžta kaip Pavyzdinė medžiaga. Licenciatas gali kopijuoti ir modifikuoti Šaltinio komponentus ir Pavyzdinę medžiagą tik naudoti viduje, jei toks naudojimas nepažeidžia šios Sutarties licencijos teisių ir jei Licenciatas nekeičia ar nenaikina jokios Šaltinio komponentuose ir Pavyzdinėje medžiagoje esančios autoriaus teisių informacijos ar pranešimų. IBM pateikia Šaltinio komponentus ir Pavyzdinę medžiagą be palaikymo įsipareigojimo ir TOKIĄ, KOKIA YRA, NESUTEIKIANT JOKIŲ AIŠKIAI NURODYTŲ AR NENURODYTŲ GARANTIJŲ, ĮSKAITANT NUOSAVYBĖS TEISĖS, NEPAŽEIDŽIAMUMO AR NEĮSIKIŠIMO GARANTIJAS IR NENURODYTAS TINKAMUMO PREKIAUTI IR TAM TIKRAM TIKSLUI GARANTIJAS IR SĄLYGAS.

L/N:  L-APIG-9RMFZQ
D/N:  L-APIG-9RMFZQ
P/N:  L-APIG-9RMFZQ 