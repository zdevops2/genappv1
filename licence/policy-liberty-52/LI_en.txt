LICENSE INFORMATION

The Programs listed below are licensed under the following License Information terms and conditions in addition to the Program license terms previously agreed to by Client and IBM. If Client does not have previously agreed to license terms in effect for the Program, the IBM International License Agreement for Non-Warranted Programs (Z125-5589-05) applies.

Program Name: IBM CICS TS GENAPP extension using Liberty Profile Version: 5 Release: 1
Program Number: SupportPac CB12

Limited Use Program

This Program is supplied only for use with Named Program(s) identified below or their successors. Licensee is prohibited from using this Program in connection with any other software.

Named Program(s):
IBM CICS Transaction Server for z/OS V5

Source Components and Sample Materials

The Program may include some components in source code form ("Source Components") and other materials identified as Sample Materials. Licensee may copy and modify Source Components and Sample Materials for internal use only provided such use is within the limits of the license rights under this Agreement, provided however that Licensee may not alter or delete any copyright information or notices contained in the Source Components or Sample Materials. IBM provides the Source Components and Sample Materials without obligation of support and "AS IS", WITH NO WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING THE WARRANTY OF TITLE, NON-INFRINGEMENT OR NON-INTERFERENCE AND THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

L/N:  L-APIG-9RMFZQ
D/N:  L-APIG-9RMFZQ
P/N:  L-APIG-9RMFZQ 