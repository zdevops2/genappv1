package com.ibm.db2;

import java.sql.*;
import java.lang.String;

public class QueryPetD {
    public static void queryPetDResult(ResultSet[] rs) throws SQLException, Exception {
        Connection conndb2 = null;
        String sql = " ";
        Statement stmtdb2 = null;
        try {
            conndb2 = DriverManager.getConnection("jdbc:default:connection");
            sql = "SELECT NAME, SPECIES, BREED, COLOR "
                + "FROM PET";
            stmtdb2 = conndb2.createStatement();
            rs[0] = stmtdb2.executeQuery(sql) ;
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (conndb2 != null)
                conndb2.close();
            throw(e);
        }
    }
}
